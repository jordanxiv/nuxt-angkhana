const URL = "http://localhost:3000";

export default {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "stylesheet", href: "/css/font-awesome/css/all.css" }
      // { rel: "stylesheet", href: "/css/page.min.css" }
    ],
    script: [
      { src: "https://use.fontawesome.com/releases/v5.11.2/js/all.js" }
      // { src: "https://code.jquery.com/jquery-3.3.1.min.js", body: true },
      // {
      //   src:
      //     "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
      //   body: true
      // },
      // {
      //   src:
      //     "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js",
      //   body: true
      // }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#03a87c", failedColor: "#03a87c" },
  /*
   ** Global CSS
   */
  css: ["@/assets/css/default.scss"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ["@plugins/vuetify"],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/vuetify"],
  vuetify: {
    customVariables: ["~/assets/variables.scss"]
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    "bootstrap-vue/nuxt",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "nuxt-material-design-icons"
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true,
    baseURL: URL
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    extractCSS: true
  },
  router: {
    linkExactActiveClass: "exact-active-link"
  }
};
