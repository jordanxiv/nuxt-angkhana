import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import colors from "vuetify/es5/util/colors";
//import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader version "^2.1.1" ,

Vue.use(Vuetify);

export default ctx => {
  const vuetify = new Vuetify({
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: false, // From 2.0 You have to select the theme dark or light here
      themes: {
        light: {
          primary: colors.deepPurple.lighten3,
          accent: colors.deepPurple.accent3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.teal.darken1
        }
      }
    }
  });

  ctx.app.vuetify = vuetify;
  ctx.$vuetify = vuetify.framework;
};
