import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _63fd7cf2 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _7b0cd416 = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _0360ee0e = () => interopDefault(import('../pages/course/create.vue' /* webpackChunkName: "pages/course/create" */))
const _6cf208d5 = () => interopDefault(import('../pages/teacher/draft/index.vue' /* webpackChunkName: "pages/teacher/draft/index" */))
const _ed4ebf3c = () => interopDefault(import('../pages/teacher/published/index.vue' /* webpackChunkName: "pages/teacher/published/index" */))
const _4bc8f1d0 = () => interopDefault(import('../pages/course/_id/update.vue' /* webpackChunkName: "pages/course/_id/update" */))
const _03ed5642 = () => interopDefault(import('../pages/course/_id/videos.vue' /* webpackChunkName: "pages/course/_id/videos" */))
const _04599e70 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'exact-active-link',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _63fd7cf2,
    name: "login"
  }, {
    path: "/register",
    component: _7b0cd416,
    name: "register"
  }, {
    path: "/course/create",
    component: _0360ee0e,
    name: "course-create"
  }, {
    path: "/teacher/draft",
    component: _6cf208d5,
    name: "teacher-draft"
  }, {
    path: "/teacher/published",
    component: _ed4ebf3c,
    name: "teacher-published"
  }, {
    path: "/course/:id?/update",
    component: _4bc8f1d0,
    name: "course-id-update"
  }, {
    path: "/course/:id?/videos",
    component: _03ed5642,
    name: "course-id-videos"
  }, {
    path: "/",
    component: _04599e70,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
