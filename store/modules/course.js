const state = () => ({
  courseTitle: "My Course",
  isUpdate: false,
  formCounter: 8, // 8 is # of current input in create form
  totalDuration: 0,
  durationCheck: false,
  publicable: false
});

const getters = {
  courseTitle: state => {
    return state.courseTitle;
  },
  isUpdate: state => {
    return state.isUpdate;
  },
  formCounter: state => {
    return state.formCounter;
  },
  durationCheck: state => {
    return state.durationCheck;
  },
  totalDuration: state => {
    return state.totalDuration;
  },
  publicable: state => {
    return state.publicable;
  }
};

const mutations = {
  updateTitle: (state, payload) => {
    state.courseTitle = payload ? payload : "My Course";
  },
  resetTitle: state => {
    state.courseTitle = "My Course";
  },
  updating: state => {
    state.isUpdate = true;
  },
  updated: state => {
    state.isUpdate = false;
    state.publicable = state.formCounter === 0 && state.durationCheck;
  },
  decrementFormCouter: (state, payload) => {
    state.formCounter = 7 - payload; // 8 is # of current input in create form
    state.publicable = state.formCounter === 0 && state.durationCheck;
  },
  videosDuration: (state, payload) => {
    state.durationCheck = payload >= 60; // course length >= 60 sec
  },
  addDuration: (state, payload) => {
    state.totalDuration += payload;
    state.publicable = state.formCounter === 0 && state.durationCheck;
  }
};

const actions = {
  updateTitle: ({ commit }, payload) => {
    commit("updateTitle", payload);
  },
  resetTitle: ({ commit }) => {
    commit("resetTitle");
  },
  updating: ({ commit }) => {
    commit("updating");
  },
  updated: ({ commit }) => {
    commit("updated");
  },
  decrementFormCouter: ({ commit }, payload) => {
    commit("decrementFormCouter", payload);
  },
  videosDuration: ({ commit }, payload) => {
    commit("videosDuration", payload);
  },
  addDuration: ({ commit }, payload) => {
    commit("addDuration", payload);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
