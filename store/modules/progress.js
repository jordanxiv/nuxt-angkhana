const state = () => ({
    progress: 0
})

const getters = () => ({
    progress: state => state.progress
})

const mutations = () => ({
    updateProgress: (state, action) => {
        state.progress = action.progress;
    },
    clearProgress: (state) => {
        state.progress = 0;
    },
})

const actions = () => ({
    updateProgress: ({ commit }, payload) => {
        commit('updateProgress', {
            progress: payload.progress
        })
    },
    clearProgress: ({ commit }) => {
        commit('clearProgress')
    },
})

export default {
    state,
    getters,
    mutations,
    actions
}