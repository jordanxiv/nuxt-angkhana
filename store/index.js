import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

import progress from "./modules/progress";
import course from "./modules/course";

export const state = () => ({
  categories: null
});

export const getters = {};

export const mutations = {
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
  }
};

export const actions = {
  // Initial datas
  async nuxtServerInit({ commit }) {
    try {
      const res = await axios.post("http://localhost:3000/api/get-categories");
      const categories = res.data.data;

      commit("SET_CATEGORIES", categories);
    } catch (error) {
      console.log(error);
    }
  }
};

export const modules = {
  course,
  progress
};
