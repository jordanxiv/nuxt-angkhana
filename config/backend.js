import axios from "axios";
import endPoint from "../config/endPoint";

const passResponse = response => {
    return response.data;
};

const api = axios.create({
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
});

const apiAuth = axios.create({
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: localStorage.getItem('token')
    }
});

// const apiProgressAuth = axios.create({
//     headers: {
//         'Content-Type': 'multipart/form-data',
//         Authorization: localStorage.getItem('token')
//     },
//     onUploadProgress: function (progressEvent) {
//         const { loaded, total } = progressEvent;
//         var percentCompleted = Math.ceil((loaded / total) * 100);
//     }
// });

const backend = {
    login: (email, password) =>
        api
            .post(endPoint.login, {
                email: email,
                password: password
            })
            .then(passResponse),

    logout: () =>
        apiAuth
            .post(endPoint.logout, {code:"I7CSE6xSx2"})
            .then(passResponse)
}

export default backend;
