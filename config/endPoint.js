const url = 'http://localhost:3000/api'

export default {
  // AUTH
  register: `${url}/register`,
  login: "http://localhost:3000/api/login",
  logout: "http://localhost:3000/api/logout",

  videoUpload: "http://localhost:3000/video-upload",
  uploadFileFirebase:
    "https://us-central1-angkhana-5805c.cloudfunctions.net/uploadFile",

  // other
  getLevels: "http://localhost:3000/get-levels",
  getSkills: "http://localhost:3000/get-skills",
  getPricing: "http://localhost:3000/get-pricing",
  getCategories: "http://localhost:3000/get-categories",
  getSubCategories: `http://localhost:3000/get-subcategories`,
  createCourse: `http://localhost:3000/create-course`,
  getCourse: `http://localhost:3000/get-course`,
  updataCourse: `http://localhost:3000/update-course`,
  getVideos: `http://localhost:3000/get-videos`,
  updateVideoIndex: `http://localhost:3000/update-video-index`,
  updateVideo: `http://localhost:3000/update-video`,
  deleteVideo: `http://localhost:3000/delete-video`
};
